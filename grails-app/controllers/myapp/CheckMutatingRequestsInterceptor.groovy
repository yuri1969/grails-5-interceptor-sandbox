package myapp

import grails.web.http.HttpHeaders
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

import javax.servlet.http.HttpServletRequest

@Slf4j
@CompileStatic()
class CheckMutatingRequestsInterceptor {

    public CheckMutatingRequestsInterceptor() {
        match(method: ~/(PUT|PATCH|DELETE)/)
    }

    boolean before() {
        log.info('Intercepting...')

        if(!containsIfMatch(request)) {
            render view: '/missingIfMatch'
            return false
        }

        return true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }

    protected boolean containsIfMatch(final HttpServletRequest request) {
        final String ifMatch = request.getHeader(HttpHeaders.IF_MATCH)

        log.info("If-Match value was: '${ifMatch}'")

        return ifMatch && ifMatch =~ /^"\S+"$/
    }
}
