package myapp

import grails.testing.mixin.integration.Integration
import grails.testing.spock.OnceBefore
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import static io.micronaut.http.HttpHeaders.IF_MATCH

@Integration()
class CheckMutatingRequestsInterceptorFunctionalSpec extends Specification {

    @Shared
    @AutoCleanup
    HttpClient client

    @OnceBefore
    void init() {
        String baseUrl = "http://localhost:$serverPort"
        this.client = HttpClient.create(new URL(baseUrl))

        // prepare data
        client.toBlocking().exchange(HttpRequest.POST('/book', new Book(name: 'foo')), Book)
    }

    void "Test checkMutatingRequests interceptor before method doesn't harass a GET"() {
        when: "Launch a plain GET request"
        final HttpResponse<List> response = client.toBlocking().exchange(HttpRequest.GET('/book'), List)

        then: "The response is not affected"
        response.status == HttpStatus.OK
        response.header(HttpHeaders.CONTENT_TYPE) == 'application/json;charset=UTF-8'
    }

    void "Test checkMutatingRequests interceptor before method rejects missing If-Match"() {
        when: "Prepare a PUT request to a controller with no If-Match header"
        client.toBlocking().exchange(HttpRequest.PUT('/book/1', new Book(name: 'bar')), Book)

        then:
        final HttpClientResponseException ex = thrown()
        and: "The interceptor before method should reject the request"
        ex.status == HttpStatus.BAD_REQUEST
    }

    void "Test checkMutatingRequests interceptor before method accepts correct If-Match value"(final String ifMatch, final HttpStatus status) {
        given: "Prepare a PUT request to a controller with a If-Match header"
        final HttpRequest request = HttpRequest.PUT('/book/1', new Book(name: 'bar')).header(IF_MATCH, ifMatch)
        and: "Issue the request"
        final HttpResponse<Book> response = client.toBlocking().exchange(request, Book)

        expect: "The interceptor before method should accept the request"
        response.status == status

        where:
        ifMatch   | status
        '"1234"'  | HttpStatus.OK
        '"12bar"' | HttpStatus.OK
        '"f"'     | HttpStatus.OK
    }

    void "Test checkMutatingRequests interceptor before method doesn't accept wrong If-Match value"(final String ifMatch, final HttpStatus status) {
        when: "Prepare a PUT request to a controller with wrong If-Match header"
        final HttpRequest request = HttpRequest.PUT('/book/1', new Book(name: 'bar')).header(IF_MATCH, ifMatch)
        and: "Issue the request"
        client.toBlocking().exchange(request, Book)

        then:
        final HttpClientResponseException ex = thrown()
        expect: "The interceptor before method should reject the request"
        ex.status == status

        where:
        ifMatch | status
        '1234'  | HttpStatus.BAD_REQUEST
        ''      | HttpStatus.BAD_REQUEST
        '" "'   | HttpStatus.BAD_REQUEST
    }
}
