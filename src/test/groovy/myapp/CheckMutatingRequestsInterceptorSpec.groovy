package myapp

import grails.testing.web.interceptor.InterceptorUnitTest
import spock.lang.Specification

class CheckMutatingRequestsInterceptorSpec extends Specification implements InterceptorUnitTest<CheckMutatingRequestsInterceptor> {

    def setup() {
    }

    def cleanup() {
    }

    void "Test checkMutatingRequests interceptor matches a request by method"(final String method, final boolean matches) {
        given: "Prepare request to an unspecified controller"
        withRequest(controller: 'test')
        and: "Prepare request to given method"
        interceptor.request.method = method

        expect: "The interceptor match should behave as given"
        interceptor.doesMatch() == matches

        where:
        method    | matches
        'POST'    | false
        'GET'     | false
        'PUT'     | true
        'PATCH'   | true
        'DELETE'  | true
        'OPTIONS' | false
        'HEAD'    | false
    }
}
